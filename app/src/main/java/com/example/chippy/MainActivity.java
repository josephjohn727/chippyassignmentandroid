package com.example.chippy;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

public class MainActivity extends AppCompatActivity {
    GameEngine chippyGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set No Title
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Get size of the screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Initialize the GameEngine object
        // Pass it the screen size (height & width)
        chippyGame = new GameEngine(this, size.x, size.y);

        setContentView(chippyGame);
    }

    // Android Lifecycle function
    @Override
    protected void onResume() {
        super.onResume();
        chippyGame.startGame();
    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        chippyGame.pauseGame();
    }
}
