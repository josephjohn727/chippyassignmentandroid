package com.example.chippy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Fire {

    private int xPosition;
    private int yPosition;
    Context context;
    private Bitmap image;

    public Fire(Context context, int xPosition, int yPosition) {
        this.context = context;
        this.xPosition = xPosition;
        this.yPosition = yPosition;

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.target);
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
