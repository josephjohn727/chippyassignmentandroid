package com.example.chippy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private int xPosition;
    private int yPosition;
    private Bitmap image;
    Context context;
    private Rect hitbox;
    private String action;
    //private ArrayList<Rect> bullets = new ArrayList<Rect>();
    ArrayList<Bullet> bullets = new ArrayList<Bullet>();
    private final int BULLET_WIDTH = 15;

    private int SQUARE_WIDTH = 10;

    private int powerupX = 200;
    private int powerupY = 1200;

    private int lives;

    private List<Rect> powerUpRects;

    public Player(Context context,int xPosition, int yPosition, String action) {
        this.context = context;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.action = action;

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.spaceship);

        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.image.getWidth(),
                this.yPosition + this.image.getHeight()
        );

        powerUpRects = new ArrayList<>();
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }

    public void setBullets(ArrayList<Bullet> bullets) {
        this.bullets = bullets;
    }

    public int getPowerupX() {
        return powerupX;
    }

    public void setPowerupX(int powerupX) {
        this.powerupX = powerupX;
    }

    public int getPowerupY() {
        return powerupY;
    }

    public void setPowerupY(int powerupY) {
        this.powerupY = powerupY;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public List<Rect> getPowerUpRects() {
        return powerUpRects;
    }

    public void setPowerUpRects(List<Rect> powerUpRects) {
        this.powerUpRects = powerUpRects;
    }

    public void updateHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.image.getWidth();
        this.hitbox.bottom = this.yPosition + this.image.getHeight();
    }

    // Make a new bullet
//    public void spawnBullet() {
//        // make bullet come out of middle of enemty
//        Rect bullet = new Rect(this.xPosition + image.getWidth() + BULLET_WIDTH,
//                this.yPosition + this.image.getHeight() / 2
//                ,this.xPosition + image.getWidth(),
//                this.yPosition + this.image.getHeight() / 2 + BULLET_WIDTH
//        );
//        this.bullets.add(bullet);
//    }

    public  void bulletGenerate(){
//        for (int i = 1; i < 2; i++) {
        Bullet b = new Bullet(context, xPosition+25, yPosition+25, SQUARE_WIDTH, 100);
        this.bullets.add(b);
        // }
    }

    //PowerUps creation
    public List<Rect> createPowerups(){
        List<Rect> rects = new ArrayList<>();

        for (int i = 0;i < 2;i++){
            Rect rect = new Rect(powerupX +50  , powerupY- 400 * i - 310 , powerupX  ,powerupY - 250 - 400*i);


            rects.add(rect);
        }
        return rects;
    }
}
