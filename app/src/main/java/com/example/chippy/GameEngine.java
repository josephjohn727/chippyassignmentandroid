package com.example.chippy;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Chronometer;

import java.util.ArrayList;
import java.util.List;

import rb.popview.PopField;

public class GameEngine extends SurfaceView implements Runnable {

    // screen size
    int screenHeight;
    int screenWidth;

    Thread gameThread;
    boolean gameIsRunning;

    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    Context context;

    int rectXPosition;
    int rectYPosition;

    EnemyBlocks enemyBlocks;
    Enemy enemy;

    Direction directionUp;
    Direction directionDown;
    Direction directionRight;
    Direction directionLeft;
    Fire fire;

    Player player;

    boolean isMoving = false;

    private double xn;
    private double yn;

    int numLoops = 0;

    private List<Rect> surroudingRects;
    private List<Rect> coreRects;

    private boolean isSurroundingHit = false;
    private boolean isCoreHit = false;

    private Rect surroundRect;
    private Rect coreRect;

    private boolean isYellowSurroundHit = false;
    private  boolean isBlueCoreHit = false;

    //Global dec
    int environmentX = 200;
    int environmentY = 1200;
    int environmentLoop =0;
    // Array of Bullets
    //ArrayList<Bullet> bullets = new ArrayList<Bullet>();
    private Chronometer chronometer;

    int SQUARE_WIDTH = 10;

    PopField popField;

    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();
        this.context = context;

        chronometer = new Chronometer(context);

        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();

        surroudingRects = new ArrayList<>();
        coreRects = new ArrayList<>();

        popField = PopField.attach2Window((Activity) context);

        this.screenWidth = w;
        this.screenHeight = h;

        rectXPosition = screenWidth / 2;
        rectYPosition = screenHeight / 2;

        player = new Player(context, 100, screenHeight / 2, "");

        enemyBlocks = new EnemyBlocks(context, rectXPosition, rectYPosition);
        enemy = new Enemy(context, rectXPosition, rectYPosition);

        player.setLives(5);


        this.directionUp = new Direction(getContext(), screenWidth - 400, screenHeight - 500);
        this.directionDown = new Direction(getContext(), screenWidth - 400, screenHeight - 300);
        this.directionLeft = new Direction(getContext(), screenWidth - 500, screenHeight - 400);
        this.directionRight = new Direction(getContext(), screenWidth - 300, screenHeight - 400);

        this.fire =new Fire(getContext(),100,screenHeight-400);

    }


    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }

    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void updatePositions() {
        // 3. move the bullet
        int x = (int) (xn * 15);
        int y = (int) (yn * 15);

//        int x1 = (int) (xn * 5);
//        int y1 = (int) (yn * 5);

        numLoops = numLoops + 1;



        enemyBlocks.setRadius(enemyBlocks.getRadius() + 20);

        //enemy.setLineXPosition(enemy.getLineXPosition() + x);
        //enemy.setLineYPosition(enemy.getLineYPosition() + y);

        if (!isMoving) {

            enemyBlocks.setxCoord(enemyBlocks.getxCoord() + 30);
            enemyBlocks.setyCoord(enemyBlocks.getyCoord() + 30);
            isMoving = true;

        } else if (isMoving) {
            enemyBlocks.setxCoord(enemyBlocks.getxCoord() - 30);
            enemyBlocks.setyCoord(enemyBlocks.getyCoord() - 30);
            isMoving = false;
        }

        if (player.getAction() == "up") {
            // if mouseup, then move player up
            player.setyPosition(player.getyPosition() - 100);
            player.updateHitbox();
            player.setAction("");
        } else if (player.getAction() == "down") {
            // if mousedown, then move player down
            player.setyPosition(player.getyPosition() + 100);
            player.updateHitbox();
            player.setAction("");

        } else if (player.getAction() == "left") {
            // if mouseleft, then move player left
            player.setxPosition(player.getxPosition() - 100);
            player.updateHitbox();
            player.setAction("");

        } else if (player.getAction() == "right") {
            // if mouseright, then move player right
            player.setxPosition(player.getxPosition() + 100);
            player.updateHitbox();
            player.setAction("");
        }



        for (int i = 0; i < this.player.getBullets().size();i++) {
            Bullet bullet = this.player.getBullets().get(i);

            if (surroudingRects.size() != 0){
                checkCollisionSurroundingRects(bullet);
            }else if (coreRects.size() != 0){
                checkCollisionCoreRects(bullet);
            }else {
                checkInitialCollisionSurroundingRects(bullet);
                checkInitialCollisionCoreRects(bullet);

            }

        }

        if (enemy.getEnvironmentRects().size() != 0){
            for (int i = 0; i < this.enemy.getEnvironmentRects().size();i++) {
                Rect environmentRect = this.enemy.getEnvironmentRects().get(i);

                if (player.getHitbox().intersect(environmentRect)){
                    player.setLives(player.getLives() - 1);
                }

            }
        }

        if (player.getPowerUpRects().size() != 0){
            for (int i = 0; i < this.player.getPowerUpRects().size();i++) {
                Rect powerUpRect = this.player.getPowerUpRects().get(i);

                if (player.getHitbox().intersect(powerUpRect)){
                    player.setLives(player.getLives() + 1);
                }

            }
        }

        if (enemyBlocks.getEnemyMapCopy() != null && enemyBlocks.getEnemyMapCopy().get(0) != null && enemyBlocks.getEnemyMapCopy().get(0).size() != 0){
            for (int i = 0; i < enemyBlocks.getEnemyMapCopy().get(0).size(); i++) {
                Rect fireRect = this.enemyBlocks.getEnemyMapCopy().get(0).get(i).getHitbox();

                if (player.getHitbox().intersect(fireRect)){
                    player.setLives(player.getLives() - 1);
                }

            }
        }

        //Move environment obstacles
        this.environmentLoop = this.environmentLoop + 1;

        enemy.setEnvironmentX(enemy.getEnvironmentX() + 30);
        if(enemy.getEnvironmentX() > screenWidth){
            enemy.setEnvironmentX(200);
        }

        player.setPowerupX(player.getPowerupX() + 30);
        if(player.getPowerupX() > screenWidth){
            player.setPowerupX(200);
        }

        if (player.getLives() == 0){
            pauseGame();
        }


    }

    private void checkCollisionSurroundingRects(Bullet bullet) {
        for (int i = 0; i < this.surroudingRects.size();i++) {
            if (bullet.getHitbox().intersect(surroudingRects.get(i)) == true){
                isYellowSurroundHit = true;
                enemyBlocks.getSurroundingChips().remove(surroudingRects.get(i));
                surroudingRects.remove(surroudingRects.get(i));
                break;
            }
        }
    }

    private void checkCollisionCoreRects(Bullet bullet) {
        for (int i = 0; i < this.coreRects.size();i++) {
            if (bullet.getHitbox().intersect(coreRects.get(i)) == true){
                isBlueCoreHit = true;
                enemyBlocks.getCoreChips().remove(coreRects.get(i));
                coreRects.remove(coreRects.get(i));
                break;
            }
        }
    }

    private void checkInitialCollisionSurroundingRects(Bullet bullet) {
        for (int i = 0; i < this.enemyBlocks.getSurroundingChips().size(); i++) {
            if (bullet.getHitbox().intersect(enemyBlocks.getSurroundingChips().get(i)) == true){
                Log.d("Clicked","true");
                surroundRect = new Rect(enemyBlocks.getSurroundingChips().get(i).left,
                        enemyBlocks.getSurroundingChips().get(i).top,
                        enemyBlocks.getSurroundingChips().get(i).right,
                        enemyBlocks.getSurroundingChips().get(i).bottom);
                isSurroundingHit = true;
                surroudingRects.add(enemyBlocks.getSurroundingChips().get(i));
            }
        }
    }

    private void checkInitialCollisionCoreRects(Bullet bullet) {
        for (int i = 0; i < this.enemyBlocks.getCoreChips().size(); i++) {
            if (bullet.getHitbox().intersect(enemyBlocks.getCoreChips().get(i)) == true){
                coreRect = new Rect(enemyBlocks.getCoreChips().get(i).left,
                        enemyBlocks.getCoreChips().get(i).top,
                        enemyBlocks.getCoreChips().get(i).right,
                        enemyBlocks.getCoreChips().get(i).bottom);
                isCoreHit = true;
                coreRects.add(enemyBlocks.getCoreChips().get(i));
            }
        }
    }

    private void calculateThePatternDistance(int getxPosition, int getyPosition) {
        double a = (getxPosition);
        double b = (getyPosition);
        double distance = Math.sqrt((a * a) + (b * b));

        // 2. calculate the "rate" to move
        xn = (a / distance);
        yn = (b / distance);

    }


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //counter = counter + 1;

            long chronometerTime = SystemClock.elapsedRealtime() - chronometer.getBase();
            String time = Long.toString(chronometerTime/1000);

            paintbrush.setColor(Color.WHITE);

            canvas.drawBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.background),0,0,null);



            setImageForUserControls();

            enemyBlocks.createEnemyBulletPattern(enemy.getxPosition(), enemy.getyPosition(), 0);

            for (int i = 0; i < enemyBlocks.getEnemyMap().get(0).size(); i++) {
                canvas.drawBitmap(enemyBlocks.getEnemyMap().get(0).get(i).getImage(), enemyBlocks.getEnemyMap().get(0).get(i).getxPosition(), enemyBlocks.getEnemyList().get(i).getyPosition(), paintbrush);
            }



            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);

            canvas.drawBitmap(directionUp.getImage(), directionUp.getxPosition(), directionUp.getyPosition(), paintbrush);
            canvas.drawBitmap(directionDown.getImage(), directionDown.getxPosition(), directionDown.getyPosition(), paintbrush);
            canvas.drawBitmap(directionLeft.getImage(), directionLeft.getxPosition(), directionLeft.getyPosition(), paintbrush);
            canvas.drawBitmap(directionRight.getImage(), directionRight.getxPosition(), directionRight.getyPosition(), paintbrush);
            canvas.drawBitmap(fire.getImage(), fire.getxPosition(), fire.getyPosition(), paintbrush);


            for (int i = 0; i < enemyBlocks.getSurroundingChips().size(); i++) {
                canvas.drawRect(enemyBlocks.getSurroundingChips().get(i), paintbrush);
            }

            paintbrush.setColor(Color.RED);



            for (int i = 0; i < enemyBlocks.getCoreChips().size(); i++) {
                canvas.drawRect(enemyBlocks.getCoreChips().get(i), paintbrush);
            }

            enemyBlocks.setFirstTime(false);

            paintbrush.setColor(Color.YELLOW);

            if (isSurroundingHit){
                canvas.drawRect(surroundRect,paintbrush);
                isSurroundingHit = false;
            }

            paintbrush.setColor(Color.CYAN);

            if (isCoreHit){
                canvas.drawRect(coreRect,paintbrush);
                isCoreHit = false;
            }

            if (isYellowSurroundHit){
                //popField.popView(yellowSurroundRect);
            }

            paintbrush.setColor(Color.YELLOW);
            paintbrush.setTextSize(100);


            canvas.drawText("Time: " + time, 100, 200,paintbrush);

            canvas.drawText("Lives: " + player.getLives() + "", 750, 200,paintbrush);

            //canvas.drawLine(rectXPosition,rectYPosition,enemy.getLineXPosition(),enemy.getLineYPosition(),paintbrush);



            paintbrush.setColor(Color.WHITE);

            paintbrush.setStrokeWidth(8);

            // draw bullet
            // paintbrush.setColor(Color.BLACK);

            for (int i = 0; i < player.getBullets().size();i++) {
                Bullet b = player.getBullets().get(i);
                canvas.drawRect(
                        b.getxPosition(),
                        b.getyPosition(),
                        b.getxPosition() + b.getWidth(),
                        b.getyPosition() + b.getWidth(),
                        paintbrush
                );
            }

            for (int i = 0; i < player.getBullets().size();i++) {
                Bullet b = player.getBullets().get(i);

                moveBullet(b);
            }

            int interval=Integer.parseInt(time);

            paintbrush.setColor(Color.RED);


            if((interval>10&&interval<20)||(interval>30&&interval<40)){
                for (int i = 0; i < enemy.createEnvironment().size(); i++) {
                    canvas.drawRect(enemy.createEnvironment().get(i), paintbrush);
                    Log.d("leftSprites", "redrawSprites: ");

                }}


            //canvas.drawBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.background),0,0,null);
            int colourPower=getResources().getColor(R.color.colorPower);
            paintbrush.setColor(colourPower);
            if((interval>20&&interval<30)||(interval>50&&interval<60)){
                for (int i = 0; i < player.createPowerups().size(); i++) {
                    canvas.drawRect(player.createPowerups().get(i), paintbrush);
                    Log.d("leftPower", "redrawPower: ");

                }}


            this.holder.unlockCanvasAndPost(canvas);

        }
    }

    private void setImageForUserControls() {
        Matrix mat = new Matrix();
        mat.postRotate(90);
        Bitmap downArrow = Bitmap.createBitmap(directionRight.getImage(), 0, 0,
                directionRight.getImage().getWidth(), directionRight.getImage().getHeight(),
                mat, true);
        directionDown.setImage(downArrow);
        Bitmap leftArrow = Bitmap.createBitmap(downArrow, 0, 0,
                downArrow.getWidth(), downArrow.getHeight(),
                mat, true);
        directionLeft.setImage(leftArrow);
        Bitmap upArrow = Bitmap.createBitmap(leftArrow, 0, 0,
                leftArrow.getWidth(), leftArrow.getHeight(),
                mat, true);
        directionUp.setImage(upArrow);
    }


    public void setFPS() {
        try {
            gameThread.sleep(20);
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {

        if (e.getY() < directionUp.getyPosition() + directionUp.getImage().getHeight() && e.getY() > directionUp.getyPosition()) {
            player.setAction("up");
        } else if (e.getY() < directionDown.getyPosition() + directionDown.getImage().getHeight() && e.getY() > directionDown.getyPosition()) {
            player.setAction("down");
        } else if (e.getX() < directionLeft.getxPosition() + directionLeft.getImage().getWidth() && e.getX() > directionLeft.getxPosition()) {
            player.setAction("left");
        } else if (e.getX() < directionRight.getxPosition() + directionRight.getImage().getWidth() && e.getX() > directionRight.getxPosition()) {
            player.setAction("right");
        }else if (e.getX() < fire.getxPosition() + fire.getImage().getWidth() && e.getX() > fire.getxPosition()) {
            player.bulletGenerate();



        }

        return true;
    }


//    }


    public void moveBullet(Bullet bulletGet) {
        // @TODO:  Move the square
        // 1. calculate distance between bullet and square
        double a = ((rectXPosition + 300) - bulletGet.xPosition);
        double b = ((rectYPosition + 100) - bulletGet.yPosition);
        double distance = Math.sqrt((a*a) + (b*b));

        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);

        // 3. move the bullet
        bulletGet.xPosition = bulletGet.xPosition + (int)(xn * bulletGet.getSpeed());
        bulletGet.yPosition = bulletGet.yPosition + (int)(yn * bulletGet.getSpeed());

        bulletGet.updateHitbox();
    }




}
